import React, { Component } from 'react'
import { Home } from 'components'
var createReactClass = require('create-react-class')

class HomeContainer extends Component{
  render () {
    return (
      <Home />
    )
  }
}

export default HomeContainer
