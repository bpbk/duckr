import React, { Component } from 'react'
import { BrowserRouter as Router, Route} from 'react-router-dom'
import { HomeContainer, AuthenticateContainer } from 'containers'
import { Navigation } from 'components'
import { container, innerContainer } from './styles.css'

class MainContainer extends Component {
  render() {
    return (
      <div className={container}>
        <div className={innerContainer}>
          <Router>
            <div>
              <Navigation isAuthed={true} />
              <Route exact path='/' component={HomeContainer} />
              <Route path='/auth' component={AuthenticateContainer} />
            </div>
          </Router>
        </div>
      </div>
    )
  }
}

export default MainContainer
